import { element, by, ElementFinder } from 'protractor';

export default class TimeEntryUpdatePage {
  pageTitle: ElementFinder = element(by.id('worktajmApp.timeEntry.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  startInput: ElementFinder = element(by.css('input#time-entry-start'));
  endInput: ElementFinder = element(by.css('input#time-entry-end'));
  commentInput: ElementFinder = element(by.css('input#time-entry-comment'));
  projectSelect: ElementFinder = element(by.css('select#time-entry-project'));
  userSelect: ElementFinder = element(by.css('select#time-entry-user'));

  getPageTitle() {
    return this.pageTitle;
  }

  setStartInput(start) {
    this.startInput.sendKeys(start);
  }

  getStartInput() {
    return this.startInput.getAttribute('value');
  }

  setEndInput(end) {
    this.endInput.sendKeys(end);
  }

  getEndInput() {
    return this.endInput.getAttribute('value');
  }

  setCommentInput(comment) {
    this.commentInput.sendKeys(comment);
  }

  getCommentInput() {
    return this.commentInput.getAttribute('value');
  }

  projectSelectLastOption() {
    this.projectSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  projectSelectOption(option) {
    this.projectSelect.sendKeys(option);
  }

  getProjectSelect() {
    return this.projectSelect;
  }

  getProjectSelectedOption() {
    return this.projectSelect.element(by.css('option:checked')).getText();
  }

  userSelectLastOption() {
    this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  userSelectOption(option) {
    this.userSelect.sendKeys(option);
  }

  getUserSelect() {
    return this.userSelect;
  }

  getUserSelectedOption() {
    return this.userSelect.element(by.css('option:checked')).getText();
  }

  save() {
    return this.saveButton.click();
  }

  cancel() {
    this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
