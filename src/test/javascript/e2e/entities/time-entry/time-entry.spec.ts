/* tslint:disable no-unused-expression */
import { browser, protractor } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import TimeEntryComponentsPage from './time-entry.page-object';
import { TimeEntryDeleteDialog } from './time-entry.page-object';
import TimeEntryUpdatePage from './time-entry-update.page-object';

const expect = chai.expect;

describe('TimeEntry e2e test', () => {
  let navBarPage: NavBarPage;
  let timeEntryUpdatePage: TimeEntryUpdatePage;
  let timeEntryComponentsPage: TimeEntryComponentsPage;
  /*let timeEntryDeleteDialog: TimeEntryDeleteDialog;*/

  before(() => {
    browser.get('/');
    navBarPage = new NavBarPage();
    navBarPage.autoSignIn();
  });

  it('should load TimeEntries', async () => {
    navBarPage.getEntityPage('time-entry');
    timeEntryComponentsPage = new TimeEntryComponentsPage();
    expect(await timeEntryComponentsPage.getTitle().getText()).to.match(/Time Entries/);
  });

  it('should load create TimeEntry page', async () => {
    timeEntryComponentsPage.clickOnCreateButton();
    timeEntryUpdatePage = new TimeEntryUpdatePage();
    expect(await timeEntryUpdatePage.getPageTitle().getAttribute('id')).to.match(/worktajmApp.timeEntry.home.createOrEditLabel/);
  });

  /* it('should create and save TimeEntries', async () => {
        timeEntryUpdatePage.setStartInput('01/01/2001' + protractor.Key.TAB + '02:30AM');
        expect(await timeEntryUpdatePage.getStartInput()).to.contain('2001-01-01T02:30');
        timeEntryUpdatePage.setEndInput('01/01/2001' + protractor.Key.TAB + '02:30AM');
        expect(await timeEntryUpdatePage.getEndInput()).to.contain('2001-01-01T02:30');
        timeEntryUpdatePage.setCommentInput('comment');
        expect(await timeEntryUpdatePage.getCommentInput()).to.match(/comment/);
        timeEntryUpdatePage.projectSelectLastOption();
        timeEntryUpdatePage.userSelectLastOption();
        await timeEntryUpdatePage.save();
        expect(await timeEntryUpdatePage.getSaveButton().isPresent()).to.be.false;
    });*/

  /* it('should delete last TimeEntry', async () => {
        timeEntryComponentsPage.waitUntilLoaded();
        const nbButtonsBeforeDelete = await timeEntryComponentsPage.countDeleteButtons();
        await timeEntryComponentsPage.clickOnLastDeleteButton();

        timeEntryDeleteDialog = new TimeEntryDeleteDialog();
        expect(await timeEntryDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/worktajmApp.timeEntry.delete.question/);
        await timeEntryDeleteDialog.clickOnConfirmButton();

        timeEntryComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
        expect(await timeEntryComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(() => {
    navBarPage.autoSignOut();
  });
});
