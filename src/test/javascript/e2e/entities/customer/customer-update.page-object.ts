import { element, by, ElementFinder } from 'protractor';

export default class CustomerUpdatePage {
  pageTitle: ElementFinder = element(by.id('worktajmApp.customer.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#customer-name'));
  domainSelect: ElementFinder = element(by.css('select#customer-domain'));

  getPageTitle() {
    return this.pageTitle;
  }

  setNameInput(name) {
    this.nameInput.sendKeys(name);
  }

  getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  domainSelectLastOption() {
    this.domainSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  domainSelectOption(option) {
    this.domainSelect.sendKeys(option);
  }

  getDomainSelect() {
    return this.domainSelect;
  }

  getDomainSelectedOption() {
    return this.domainSelect.element(by.css('option:checked')).getText();
  }

  save() {
    return this.saveButton.click();
  }

  cancel() {
    this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
