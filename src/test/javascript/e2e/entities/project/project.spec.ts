/* tslint:disable no-unused-expression */
import { browser } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import ProjectComponentsPage from './project.page-object';
import { ProjectDeleteDialog } from './project.page-object';
import ProjectUpdatePage from './project-update.page-object';

const expect = chai.expect;

describe('Project e2e test', () => {
  let navBarPage: NavBarPage;
  let projectUpdatePage: ProjectUpdatePage;
  let projectComponentsPage: ProjectComponentsPage;
  /*let projectDeleteDialog: ProjectDeleteDialog;*/

  before(() => {
    browser.get('/');
    navBarPage = new NavBarPage();
    navBarPage.autoSignIn();
  });

  it('should load Projects', async () => {
    navBarPage.getEntityPage('project');
    projectComponentsPage = new ProjectComponentsPage();
    expect(await projectComponentsPage.getTitle().getText()).to.match(/Projects/);
  });

  it('should load create Project page', async () => {
    projectComponentsPage.clickOnCreateButton();
    projectUpdatePage = new ProjectUpdatePage();
    expect(await projectUpdatePage.getPageTitle().getAttribute('id')).to.match(/worktajmApp.project.home.createOrEditLabel/);
  });

  /* it('should create and save Projects', async () => {
        projectUpdatePage.setNameInput('name');
        expect(await projectUpdatePage.getNameInput()).to.match(/name/);
        projectUpdatePage.setDescriptionInput('description');
        expect(await projectUpdatePage.getDescriptionInput()).to.match(/description/);
        projectUpdatePage.setHourlyRateInput('5');
        expect(await projectUpdatePage.getHourlyRateInput()).to.eq('5');
        // projectUpdatePage.membersSelectLastOption();
        projectUpdatePage.customerSelectLastOption();
        await projectUpdatePage.save();
        expect(await projectUpdatePage.getSaveButton().isPresent()).to.be.false;
    });*/

  /* it('should delete last Project', async () => {
        projectComponentsPage.waitUntilLoaded();
        const nbButtonsBeforeDelete = await projectComponentsPage.countDeleteButtons();
        await projectComponentsPage.clickOnLastDeleteButton();

        projectDeleteDialog = new ProjectDeleteDialog();
        expect(await projectDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/worktajmApp.project.delete.question/);
        await projectDeleteDialog.clickOnConfirmButton();

        projectComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
        expect(await projectComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(() => {
    navBarPage.autoSignOut();
  });
});
