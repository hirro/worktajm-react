import { element, by, ElementFinder } from 'protractor';

export default class ProjectUpdatePage {
  pageTitle: ElementFinder = element(by.id('worktajmApp.project.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#project-name'));
  descriptionInput: ElementFinder = element(by.css('input#project-description'));
  hourlyRateInput: ElementFinder = element(by.css('input#project-hourlyRate'));
  membersSelect: ElementFinder = element(by.css('select#project-members'));
  customerSelect: ElementFinder = element(by.css('select#project-customer'));

  getPageTitle() {
    return this.pageTitle;
  }

  setNameInput(name) {
    this.nameInput.sendKeys(name);
  }

  getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  setDescriptionInput(description) {
    this.descriptionInput.sendKeys(description);
  }

  getDescriptionInput() {
    return this.descriptionInput.getAttribute('value');
  }

  setHourlyRateInput(hourlyRate) {
    this.hourlyRateInput.sendKeys(hourlyRate);
  }

  getHourlyRateInput() {
    return this.hourlyRateInput.getAttribute('value');
  }

  membersSelectLastOption() {
    this.membersSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  membersSelectOption(option) {
    this.membersSelect.sendKeys(option);
  }

  getMembersSelect() {
    return this.membersSelect;
  }

  getMembersSelectedOption() {
    return this.membersSelect.element(by.css('option:checked')).getText();
  }

  customerSelectLastOption() {
    this.customerSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  customerSelectOption(option) {
    this.customerSelect.sendKeys(option);
  }

  getCustomerSelect() {
    return this.customerSelect;
  }

  getCustomerSelectedOption() {
    return this.customerSelect.element(by.css('option:checked')).getText();
  }

  save() {
    return this.saveButton.click();
  }

  cancel() {
    this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
