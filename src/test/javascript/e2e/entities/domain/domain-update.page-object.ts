import { element, by, ElementFinder } from 'protractor';

export default class DomainUpdatePage {
  pageTitle: ElementFinder = element(by.id('worktajmApp.domain.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#domain-name'));
  membersSelect: ElementFinder = element(by.css('select#domain-members'));

  getPageTitle() {
    return this.pageTitle;
  }

  setNameInput(name) {
    this.nameInput.sendKeys(name);
  }

  getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  membersSelectLastOption() {
    this.membersSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  membersSelectOption(option) {
    this.membersSelect.sendKeys(option);
  }

  getMembersSelect() {
    return this.membersSelect;
  }

  getMembersSelectedOption() {
    return this.membersSelect.element(by.css('option:checked')).getText();
  }

  save() {
    return this.saveButton.click();
  }

  cancel() {
    this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
