/* tslint:disable no-unused-expression */
import { browser } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import DomainComponentsPage from './domain.page-object';
import { DomainDeleteDialog } from './domain.page-object';
import DomainUpdatePage from './domain-update.page-object';

const expect = chai.expect;

describe('Domain e2e test', () => {
  let navBarPage: NavBarPage;
  let domainUpdatePage: DomainUpdatePage;
  let domainComponentsPage: DomainComponentsPage;
  let domainDeleteDialog: DomainDeleteDialog;

  before(() => {
    browser.get('/');
    navBarPage = new NavBarPage();
    navBarPage.autoSignIn();
  });

  it('should load Domains', async () => {
    navBarPage.getEntityPage('domain');
    domainComponentsPage = new DomainComponentsPage();
    expect(await domainComponentsPage.getTitle().getText()).to.match(/Domains/);
  });

  it('should load create Domain page', async () => {
    domainComponentsPage.clickOnCreateButton();
    domainUpdatePage = new DomainUpdatePage();
    expect(await domainUpdatePage.getPageTitle().getAttribute('id')).to.match(/worktajmApp.domain.home.createOrEditLabel/);
  });

  it('should create and save Domains', async () => {
    domainUpdatePage.setNameInput('name');
    expect(await domainUpdatePage.getNameInput()).to.match(/name/);
    // domainUpdatePage.membersSelectLastOption();
    await domainUpdatePage.save();
    expect(await domainUpdatePage.getSaveButton().isPresent()).to.be.false;
  });

  it('should delete last Domain', async () => {
    domainComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await domainComponentsPage.countDeleteButtons();
    await domainComponentsPage.clickOnLastDeleteButton();

    domainDeleteDialog = new DomainDeleteDialog();
    expect(await domainDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/worktajmApp.domain.delete.question/);
    await domainDeleteDialog.clickOnConfirmButton();

    domainComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await domainComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(() => {
    navBarPage.autoSignOut();
  });
});
