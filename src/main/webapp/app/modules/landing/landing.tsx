import './landing.scss';

import * as React from 'react';
import { Link } from 'react-router-dom';
import { Translate } from 'react-jhipster';
import { Button, Row, Col, Alert } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const Landing = ({ account }) => (
  <div id="landing">
    <div className="jumbotron" id="introduction">
      <div className="container">
        <h1 className="display-4">Worktajm</h1>
        <p className="lead">Time reporting</p>
        <p>
          <Button color="primary" size="lg" tag={Link} to="/login">
            Sign in
          </Button>
          &nbsp;
          <Button color="primary" size="lg" tag={Link} to="/register">
            Register new user
          </Button>
        </p>
      </div>
    </div>
    <div className="jumbotron" id="features">
      <div className="container">
        <h1 className="display-4">Features</h1>
        <p className="lead">Easy to use time reporting system</p>
        <Row>
          <Col md={4}>
            <span className="fa-stack fa-4x">
              <FontAwesomeIcon icon="circle" className="fa-stack-2x" />
              <FontAwesomeIcon icon="mobile" className="fa-stack-1x fa-inverse" />
            </span>
            <h4>Mobile first</h4>
            <p className="text-muted ">The web application is works with any modern browser on any device.</p>
          </Col>
          <Col md={4}>
            <span className="fa-stack fa-4x">
              <FontAwesomeIcon icon="circle" className="fa-stack-2x" />
              <FontAwesomeIcon icon="clock" className="fa-stack-1x fa-inverse" />
            </span>
            <h4>Free and Easy to use</h4>
            <p className="text-muted ">Just register and you can start filing in your reports right now.</p>
          </Col>
          <Col md={4}>
            <span className="fa-stack fa-4x">
              <FontAwesomeIcon icon="circle" className="fa-stack-2x" />
              <FontAwesomeIcon icon="chart-bar" className="fa-stack-1x fa-inverse" />
            </span>
            <h4>Charts</h4>
            <p className="text-muted ">Get a summary of your filed hour weekly or monthly.</p>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <span className="fa-stack fa-4x">
              <FontAwesomeIcon icon="circle" className="fa-stack-2x" />
              <FontAwesomeIcon icon="desktop" className="fa-stack-1x fa-inverse" />
            </span>
            <h4>Responsive Design</h4>
            <p className="text-muted ">.</p>
          </Col>
          <Col md={4}>
            <span className="fa-stack fa-4x">
              <FontAwesomeIcon icon="circle" className="fa-stack-2x" />
              <FontAwesomeIcon icon="dollar-sign" className="fa-stack-1x fa-inverse" />
            </span>
            <h4>Free</h4>
            <p className="text-muted ">Give something to charity if you feel like it, but this app is open source and free.</p>
          </Col>
          <Col md={4}>
            <span className="fa-stack fa-4x">
              <FontAwesomeIcon icon="circle" className="fa-stack-2x" />
              <FontAwesomeIcon icon="envelope" className="fa-stack-1x fa-inverse" />
            </span>
            <h4>Reports via email</h4>
            <p className="text-muted ">Get an email of your billed hours on daily, weekly or monthly basis.</p>
          </Col>
        </Row>
      </div>
    </div>
    <div className="jumbotron" id="techstack">
      <div className="container">
        <h1 className="display-4">Technology Stack</h1>
        <Button target="_blank" href="https://stackshare.io/arnell-consulting-ab/worktajm">
          View Technology stack on Stackshare &nbsp;
          <FontAwesomeIcon icon="external-link-alt" />
        </Button>
      </div>
    </div>
  </div>
);

export default Landing;
