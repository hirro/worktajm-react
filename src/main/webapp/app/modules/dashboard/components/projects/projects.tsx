import * as React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';

import {
  Button,
  ButtonDropdown,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  CardTitle,
  CardText,
  Col,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Table,
  Row
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IProject } from 'app/shared/model/project.model';
import { ICustomer } from 'app/shared/model/customer.model';
import Project from 'app/modules/dashboard/components/projects/project';
import { Translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import {
  IStartProjectCallback,
  IStopProjectCallback,
  loadTimeEntries,
  loadUserDetails,
  setSelectedDate,
  startProject,
  stopProject
} from 'app/modules/dashboard/dashboard.reducer';
import { getSession } from 'app/shared/reducers/authentication';
import { deleteEntity as deleteTimeEntryEntity } from 'app/entities/time-entry/time-entry.reducer';
import { getEntities as getProjectEntities } from 'app/entities/project/project.reducer';

interface IProjectsProps extends RouteComponentProps<{ url: string }> {
  projects: IProject[];
  customers: ICustomer[];
  userDetails: any;
  startProject: IStartProjectCallback;
  stopProject: IStopProjectCallback;
  selectedCustomer: ICustomer;
}

interface IProjectsState {
  dropdownOpen: boolean;
  toggle: boolean;
  selectedCustomer: ICustomer;
}

export class Projects extends React.Component<IProjectsProps, IProjectsState> {
  state: IProjectsState = {
    dropdownOpen: false,
    toggle: false,
    selectedCustomer: null
  };

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  getSelectedCustomerName(): string {
    const { selectedCustomer } = this.props;
    if (!selectedCustomer) {
      return 'More';
    } else if (!selectedCustomer) {
      return selectedCustomer.name;
    }
  }

  render() {
    const { userDetails, projects, match, selectedCustomer, customers } = this.props;
    const { dropdownOpen } = this.state;
    const activeTimeEntry = userDetails ? userDetails.activeTimeEntry : null;
    const activeProject = activeTimeEntry && activeTimeEntry.project ? activeTimeEntry.project : null;
    const activeProjectId = activeProject ? activeProject.id : null;

    if (!projects) {
      return <div>Loading projects ...</div>;
    }
    if (!customers) {
      return <div>Loading customers ...</div>;
    }

    return (
      <Card id="projecsCard">
        <CardHeader>Projects</CardHeader>
        <CardBody>
          <Row>
            <ButtonDropdown isOpen={dropdownOpen} toggle={this.toggle} className="btn-block">
              <DropdownToggle caret color="primary" className="btn-block text-left" size="sm">
                {this.getSelectedCustomerName()}
              </DropdownToggle>
              <DropdownMenu>
                {customers ? <DropdownItem header>Current customer</DropdownItem> : null}
                {customers ? customers.map(customer => <DropdownItem key={customer.id}>{customer.name}</DropdownItem>) : null}
                <DropdownItem divider />
                <DropdownItem href={`${match}/new-customer`}>
                  <Translate contentKey="worktajmApp.customer.home.createLabel">Create new Customer</Translate>
                </DropdownItem>
                <DropdownItem href={`${match}/new-project`}>
                  <Translate contentKey="worktajmApp.project.home.createLabel">Create new Project</Translate>
                </DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
          </Row>
          <Row mt-2>
            <Table className="table-striped">
              <tbody>
                {projects.map(project => (
                  <tr key={project.id}>
                    <td>{project.name} </td>
                    <td className="text-right">
                      <Project
                        startProject={this.props.startProject}
                        stopProject={this.props.stopProject}
                        active={project.id === activeProjectId}
                        project={project}
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Row>
        </CardBody>
      </Card>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  userDetails: storeState.dashboard.userDetails,
  projects: storeState.dashboard.projects.entities,
  customers: storeState.customer.entities,
  timeEntries: storeState.dashboard.timeEntries.entities,
  selectedDate: storeState.dashboard.selectedDate,
  selectedCustomer: storeState.dashboard.selectedCustomer,
  currentLocale: storeState.locale.currentLocale
});

const mapDispatchToProps = {
  getSession,
  getProjectEntities,
  loadUserDetails,
  startProject,
  stopProject,
  deleteTimeEntryEntity,
  setSelectedDate,
  loadTimeEntries
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Projects);
