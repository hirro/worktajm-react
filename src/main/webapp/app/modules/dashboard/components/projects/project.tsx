import * as React from 'react';
import { Card, Button, CardHeader, CardFooter, CardBody, CardTitle, CardText, InputGroup, InputGroupAddon, Input } from 'reactstrap';
// import 'react-datepicker/dist/react-datepicker.css';
import { IProject } from 'app/shared/model/project.model';
import { IStartProjectCallback, IStopProjectCallback } from 'app/modules/dashboard/dashboard.reducer';

interface IProjectProps {
  project: IProject;
  active: boolean;
  startProject: IStartProjectCallback;
  stopProject: IStopProjectCallback;
}

const Project: React.SFC<IProjectProps> = ({ project, active, startProject, stopProject }) => {
  function onStartProject() {
    startProject(project);
  }

  function onStopProject() {
    stopProject(project);
  }

  function activeProject() {
    return (
      <Button color="danger" size="sm" onClick={onStopProject}>
        Stop
      </Button>
    );
  }

  function passiveProject() {
    return (
      <Button color="success" size="sm" onClick={onStartProject}>
        Start
      </Button>
    );
  }

  return active ? activeProject() : passiveProject();
};

export default Project;
