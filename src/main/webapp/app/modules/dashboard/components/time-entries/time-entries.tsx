import './time-entries.scss';

import * as React from 'react';
import { connect } from 'react-redux';
import moment, { Moment } from 'moment';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import { Card, Button, ButtonGroup, CardHeader, CardFooter, CardBody, CardTitle, CardText, Table } from 'reactstrap';

import { ITimeEntry } from 'app/shared/model/time-entry.model';

import TimeEntriesHeader from 'app/modules/dashboard/components/time-entries/time-entries-header';
import TimeEntryButtonGroup from 'app/modules/dashboard/components/time-entries/time-entry-button-group';

interface ITimeEntriesPanelProps {
  timeEntries: ITimeEntry[];
  deleteTimeEntryEntity: any;
  match: any;
  currentDate: any;
  updateDateSelection: any;
  userDetails: any;
  currentLocale: any;
}

const SECONDS_PER_DAY = 60 * 60 * 24;

const TimeEntriesPanel: React.SFC<ITimeEntriesPanelProps> = ({
  timeEntries,
  deleteTimeEntryEntity,
  match,
  currentDate,
  updateDateSelection,
  currentLocale
}) => {
  function formatTime(time: Moment) {
    const timeMoment = time ? moment(time) : moment();
    return timeMoment.format('HH:mm:ss');
  }

  function formatDuration(timeEntry: ITimeEntry) {
    const fromMoment = moment(timeEntry.start);
    const toMoment = timeEntry.end ? moment(timeEntry.end) : moment();
    const diff = toMoment.diff(fromMoment);
    const duration = moment.duration(diff);
    const days = Math.floor(duration.asDays());
    const formattedDiff = moment.utc(diff).format('HH:mm:ss');

    if (days > 0) {
      return `${formattedDiff} [${days} d]`;
    } else {
      return formattedDiff;
    }
  }

  function sumOfAllTimeEntries(terms: ITimeEntry[]) {
    let sum = 0;
    for (const timeEntry of terms) {
      const fromMoment = moment(timeEntry.start);
      const toMoment = timeEntry.end ? moment(timeEntry.end) : moment();
      const diff = toMoment.diff(fromMoment);
      sum += diff;
    }

    const duration = moment.duration(sum);
    const days = Math.floor(duration.asDays());
    const formattedDiff = moment.utc(sum).format('HH:mm:ss');
    if (days > 0) {
      return `${formattedDiff} [${days} d]`;
    } else {
      return formattedDiff;
    }
  }

  return (
    <Card id="timeEntriesCard">
      <CardHeader>TimeEntries</CardHeader>
      <CardBody>
        <TimeEntriesHeader
          match={match}
          currentDate={currentDate}
          updateDateSelection={updateDateSelection}
          currentLocale={currentLocale}
        />

        <div className="row">
          <Table hover>
            <thead>
              <tr>
                <th>Project</th>
                <th>Time</th>
                <th>Duration</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {timeEntries.map(timeEntry => (
                <tr key={timeEntry.id}>
                  <td>{timeEntry.project.name} </td>
                  <td>
                    {formatTime(timeEntry.start)}
                    -
                    {formatTime(timeEntry.end)}
                  </td>
                  <td>{formatDuration(timeEntry)}</td>
                  <td className="text-right">
                    <TimeEntryButtonGroup timeEntry={timeEntry} deleteTimeEntryEntity={deleteTimeEntryEntity} match={match} />
                  </td>
                </tr>
              ))}
            </tbody>
            <tfoot>
              <tr>
                <td>Sum</td>
                <td />
                <td>{sumOfAllTimeEntries(timeEntries)}</td>
                <td />
              </tr>
            </tfoot>
          </Table>
        </div>
      </CardBody>
    </Card>
  );
};

export default TimeEntriesPanel;
