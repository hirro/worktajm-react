// tslint:disable-next-line
import 'react-datepicker/dist/react-datepicker.css';

import * as React from 'react';
import { connect } from 'react-redux';
import {
  Card,
  Col,
  Button,
  CardHeader,
  CardFooter,
  CardBody,
  CardTitle,
  CardText,
  InputGroup,
  InputGroupAddon,
  Input,
  Row
} from 'reactstrap';
import DatePicker from 'react-datepicker';
import moment, { Moment } from 'moment';
import { Link } from 'react-router-dom';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ITimeEntry } from 'app/shared/model/time-entry.model';

interface ITimeEntriesHeaderProps {
  currentDate: any;
  match: any;
  updateDateSelection: any;
  currentLocale: any;
}

const TimeEntriesHeader: React.SFC<ITimeEntriesHeaderProps> = ({ currentDate, match, updateDateSelection, currentLocale }) => {
  function format(date) {
    if (date) {
      return moment(date).format('YYYY-MM-DD');
    } else {
      return '';
    }
  }

  function dummyOnChange(date: Moment) {}

  function customInput() {
    return (
      <InputGroup className="float-right">
        <InputGroupAddon addonType="prepend">
          <span className="input-group-text">
            <FontAwesomeIcon icon="calendar-alt" />
          </span>
        </InputGroupAddon>
        <Input className="form-control" value={format(currentDate)} onChange={dummyOnChange} />
      </InputGroup>
    );
  }

  function onChange(newDate: Moment) {
    // tslint:disable-next-line
    console.log('onChange', newDate);

    updateDateSelection(newDate);
  }

  return (
    <Row id="time-entries-header">
      <Col md={6} xs={10}>
        <DatePicker
          selected={currentDate}
          customInput={customInput()}
          onChange={onChange}
          locale={currentLocale ? 'en-gb' : 'en-gb'}
          showWeekNumbers
        />
      </Col>
      <Col md={6} xs={2}>
        <Link to={`${match.url}/time-entry/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />&nbsp;
          <span className="d-none d-md-inline">
            <Translate contentKey="worktajmApp.timeEntry.home.createLabel">Create new Time Entry</Translate>
          </span>
        </Link>
      </Col>
    </Row>
  );
};

export default TimeEntriesHeader;
