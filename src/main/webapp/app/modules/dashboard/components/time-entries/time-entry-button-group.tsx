import * as React from 'react';
import { connect } from 'react-redux';
import Moment from 'react-moment';
import { Translate } from 'react-jhipster';
import {
  Card,
  Button,
  ButtonGroup,
  CardHeader,
  CardFooter,
  CardBody,
  CardTitle,
  CardText,
  Popover,
  PopoverHeader,
  PopoverBody,
  Table,
  Tooltip,
  UncontrolledTooltip
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import ClickNHold from 'react-click-n-hold';
import { ITimeEntry } from 'app/shared/model/time-entry.model';

interface ITimeEntryButtonGroupProps {
  timeEntry: ITimeEntry;
  deleteTimeEntryEntity: any;
  match: any;
}

const TimeEntryButtonGroup: React.SFC<ITimeEntryButtonGroupProps> = ({ timeEntry, deleteTimeEntryEntity, match }) => {
  let popoverOpen = false;
  let deleting = false;
  let tooltipOpen = false;
  const editButtonGroupId = 'edit-button-group-' + timeEntry.id;
  const deleteButtonGroupId = 'delete-button-group-' + timeEntry.id;
  const deleteButtonId = 'delete-button-' + timeEntry.id;

  function onDelete() {
    deleteTimeEntryEntity(timeEntry.id);
    deleting = false;
    popoverOpen = false;
  }

  function onStartDelete() {
    deleting = true;
    popoverOpen = true;
  }

  function onEnd() {
    deleting = false;
  }

  function togglePopover() {
    popoverOpen = true;
  }

  function toggleTooltip() {
    tooltipOpen = !tooltipOpen;
  }

  return (
    <ButtonGroup>
      <Button tag={Link} to={`${match.url}/time-entry/${timeEntry.id}/edit`} color="light" size="sm" id={editButtonGroupId}>
        <FontAwesomeIcon icon="pencil-alt" />{' '}
        <UncontrolledTooltip target={editButtonGroupId}>
          <Translate contentKey="entity.action.edit">Edit</Translate>
        </UncontrolledTooltip>
      </Button>
      <div id={deleteButtonGroupId}>
        <ClickNHold time={2} onClickNHold={onDelete} onStart={onStartDelete} onEnd={onEnd}>
          <span>
            <Button size="sm" id={deleteButtonGroupId} color="light">
              <FontAwesomeIcon icon="trash" />{' '}
              <UncontrolledTooltip target={deleteButtonGroupId}>
                <Translate contentKey="entity.action.delete">Delete</Translate>
              </UncontrolledTooltip>
            </Button>
            <Popover isOpen={popoverOpen} target={deleteButtonGroupId} toggle={togglePopover}>
              <PopoverHeader>Popover Title</PopoverHeader>
              <PopoverBody>
                Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.
              </PopoverBody>
            </Popover>
            <Tooltip placement="left" isOpen={tooltipOpen} target={deleteButtonGroupId} toggle={toggleTooltip}>
              Hold to delete
            </Tooltip>
          </span>
        </ClickNHold>
      </div>
    </ButtonGroup>
  );
};

export default TimeEntryButtonGroup;
