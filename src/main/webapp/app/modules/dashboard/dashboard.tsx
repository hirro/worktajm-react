import './dashboard.scss';

import * as React from 'react';
import { connect } from 'react-redux';
import { getSession } from 'app/shared/reducers/authentication';
import { Row, Col, Alert, Card, Button, CardHeader, CardFooter, CardBody, CardTitle, CardText } from 'reactstrap';
import Projects from 'app/modules/dashboard/components/projects/projects';
import TimeEntries from 'app/modules/dashboard/components/time-entries/time-entries';
import { getEntities as getProjectEntities } from 'app/entities/project/project.reducer';
import { getEntities as getCustomerEntities } from 'app/entities/customer/customer.reducer';
import { getEntities as getTimeEntryEntities, deleteEntity as deleteTimeEntryEntity } from 'app/entities/time-entry/time-entry.reducer';
import { loadTimeEntries, loadUserDetails, startProject, stopProject, setSelectedDate } from 'app/modules/dashboard/dashboard.reducer';
import { IProject } from 'app/shared/model/project.model';
import { RouteComponentProps } from 'react-router-dom';
import moment from 'moment';

export interface IDashboardProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Dashboard extends React.Component<IDashboardProps> {
  private interval: number;
  constructor(props) {
    super(props);
    this.onStartProject = this.onStartProject.bind(this);
    this.onStopProject = this.onStopProject.bind(this);
    this.onDeleteTimeEntry = this.onDeleteTimeEntry.bind(this);
    this.onDateUpdated = this.onDateUpdated.bind(this);
  }

  componentWillMount() {
    this.props.getSession();
  }

  componentDidMount() {
    if (this.props.isAuthenticated) {
      this.props.getProjectEntities();
      this.props.loadTimeEntries(this.props.selectedDate);
      this.props.loadUserDetails();
      this.props.getCustomerEntities();
    } else {
      // tslint:disable-next-line
      console.log('Should not happen!');
    }
  }

  startTimer() {
    if (this.props.userDetails.activeTimeEntry && !this.interval) {
      // tslint:disable-next-line
      console.log('Starting timer');
      this.interval = setInterval(this.tick.bind(this), 1000);
    }
  }

  stopTimer() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }

  componentWillUnmount() {
    this.stopTimer();
  }

  tick() {
    // tslint:disable-next-line
    console.log('Tick');
    this.setState({
      currentTime: moment()
    });
  }

  render() {
    const { projects, timeEntries, isAuthenticated, account, match, selectedDate, userDetails, currentLocale } = this.props;

    if (userDetails.activeTimeEntry) {
      this.startTimer();
    } else {
      this.stopTimer();
    }

    return (
      <Row>
        <Col md="12" lg="3">
          <Projects />
        </Col>
        <Col md="12" lg="9">
          <TimeEntries
            timeEntries={this.props.timeEntries}
            deleteTimeEntryEntity={this.onDeleteTimeEntry}
            match={match}
            currentDate={selectedDate.clone()}
            updateDateSelection={this.onDateUpdated}
            userDetails={userDetails}
            currentLocale={currentLocale}
          />
        </Col>
      </Row>
    );
  }

  private onStartProject(project: IProject) {
    this.props.startProject(project);
  }

  private onStopProject(project: IProject) {
    this.props.stopProject(project);
  }

  private onDeleteTimeEntry(id: number) {
    this.props.deleteTimeEntryEntity(id);
  }

  private onDateUpdated(date: moment.Moment) {
    this.props.setSelectedDate(date);
    // tslint:disable-next-line
    console.log('onDateUpdated', date);
    this.props.loadTimeEntries(date);
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  userDetails: storeState.dashboard.userDetails,
  projects: storeState.dashboard.projects.entities,
  timeEntries: storeState.dashboard.timeEntries.entities,
  selectedDate: storeState.dashboard.selectedDate,
  currentLocale: storeState.locale.currentLocale
});

const mapDispatchToProps = {
  getSession,
  getProjectEntities,
  getCustomerEntities,
  loadUserDetails,
  startProject,
  stopProject,
  deleteTimeEntryEntity,
  setSelectedDate,
  loadTimeEntries
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
