import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { ACTION_TYPES as PROJECT_ACTION_TYPES } from 'app/entities/project/project.reducer';
import axios from 'axios';
import { IProject } from 'app/shared/model/project.model';
import moment from 'moment';
import { ITimeEntry } from 'app/shared/model/time-entry.model';
import { ICrudGetAllAction } from 'react-jhipster';

export const ACTION_TYPES = {
  START_PROJECT: 'dashboard/startProject',
  STOP_PROJECT: 'dashboard/stopProject',
  USER_EXTRAS: 'user-extras',
  SET_SELECTED_DATE: 'set-selected-date',
  FETCH_TIME_ENTRY_LIST_FOR_DATE: 'dashboard/fetch-time-entry'
};

const initialState = {
  timeEntries: {
    totalItems: 0,
    entities: []
  },
  projects: {
    totalItems: 0,
    entities: []
  },
  userDetails: {},
  selectedDate: moment(),
  loading: false,
  totalItems: 0,
  selectedCustomer: null
};

export type DashboardState = Readonly<typeof initialState>;

// Reducer
export default (state: DashboardState = initialState, action): DashboardState => {
  function updateTimeEntry(timeEntry: ITimeEntry): ITimeEntry[] {
    const index = state.timeEntries.entities.findIndex((item: ITimeEntry, i: number) => {
      return item.id === timeEntry.id;
    });

    if (index !== -1) {
      state.timeEntries.entities[index] = timeEntry;
    }

    return state.timeEntries.entities;
  }

  switch (action.type) {
    case SUCCESS(PROJECT_ACTION_TYPES.FETCH_PROJECT_LIST):
      return {
        ...state,
        loading: false,
        projects: {
          totalItems: action.payload.headers['x-total-count'],
          entities: action.payload.data
        }
      };
    case SUCCESS(ACTION_TYPES.FETCH_TIME_ENTRY_LIST_FOR_DATE):
      return {
        ...state,
        timeEntries: {
          totalItems: action.payload.headers['x-total-count'],
          entities: action.payload.data
        }
      };
    case SUCCESS(ACTION_TYPES.START_PROJECT):
      const newTimeEntry = action.payload.data;
      const newEntities = [...state.timeEntries.entities, newTimeEntry];
      return {
        ...state,
        timeEntries: {
          entities: newEntities,
          totalItems: newEntities.length
        },
        userDetails: {
          activeTimeEntry: newTimeEntry
        }
      };
    case SUCCESS(ACTION_TYPES.STOP_PROJECT):
      const updatedTimeEntry: ITimeEntry = action.payload.data;
      const updatedTimeEntries: ITimeEntry[] = updateTimeEntry(updatedTimeEntry);
      return {
        ...state,
        userDetails: {
          activeTimeEntry: null
        },
        timeEntries: {
          entities: updatedTimeEntries,
          totalItems: updatedTimeEntries.length
        }
      };
    case SUCCESS(ACTION_TYPES.USER_EXTRAS):
      return {
        ...state,
        userDetails: action.payload.data
      };
    case ACTION_TYPES.SET_SELECTED_DATE:
      return {
        ...state,
        selectedDate: action.payload
      };
    default:
      return state;
  }
};

// Actions
export const startProject = (project: IProject) => ({
  type: ACTION_TYPES.START_PROJECT,
  payload: axios.post(`/api/worktajm/startProject/${project.id}`)
});
export type IStartProjectCallback = (project: IProject) => void;

export const stopProject = (project: IProject) => ({
  type: ACTION_TYPES.STOP_PROJECT,
  payload: axios.post(`/api/worktajm/stopProject/${project.id}`)
});
export type IStopProjectCallback = (project: IProject) => void;

export const loadUserDetails = () => ({
  type: ACTION_TYPES.USER_EXTRAS,
  payload: axios.get('/api/user-extras')
});

export const setSelectedDate = (date: moment.Moment) => ({
  type: ACTION_TYPES.SET_SELECTED_DATE,
  payload: date
});

export const loadTimeEntries = (selectedDate?: moment.Moment) => {
  // tslint:disable-next-line
  console.log(selectedDate);
  const apiUrl = 'api/worktajm/time-entries';
  const requestUrl = `${apiUrl}${selectedDate ? `?from=${selectedDate.toISOString()}&to=${selectedDate.toISOString()}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_TIME_ENTRY_LIST_FOR_DATE,
    payload: axios.get<ITimeEntry>(`${requestUrl}${selectedDate ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
    selectedDate: `${selectedDate}`
  };
};
