import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Dashboard from './dashboard';
import TimeEntryUpdate from './components/time-entries/time-entry-update';

const Routes = ({ match }) => (
  <div className="container-fluid view-container" id="app-view-container">
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/time-entry/new`} component={TimeEntryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/time-entry/:id/edit`} component={TimeEntryUpdate} />
      <ErrorBoundaryRoute path={`${match.url}`} component={Dashboard} />
    </Switch>
  </div>
);

export default Routes;
