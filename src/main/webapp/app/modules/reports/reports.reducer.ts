import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { ACTION_TYPES as PROJECT_ACTION_TYPES } from 'app/entities/project/project.reducer';
import { ACTION_TYPES as TIME_ENTRY_ACTION_TYPES } from 'app/entities/time-entry/time-entry.reducer';
import axios from 'axios';
import { IProject } from 'app/shared/model/project.model';
import moment from 'moment';

export const ACTION_TYPES = {
  START_PROJECT: 'dashboard/startProject',
  STOP_PROJECT: 'dashboard/stopProject',
  USER_EXTRAS: 'user-extras',
  SELECT_DATE: 'select-date'
};

const initialState = {
  timeEntries: {
    totalItems: 0,
    entities: []
  },
  projects: {
    totalItems: 0,
    entities: []
  },
  userDetails: {},
  selectedDate: moment(),
  loading: false
};

export type ReportsState = Readonly<typeof initialState>;

// Reducer
export default (state: ReportsState = initialState, action): ReportsState => {
  switch (action.type) {
    case SUCCESS(PROJECT_ACTION_TYPES.FETCH_PROJECT_LIST):
      return {
        ...state,
        loading: false,
        projects: {
          totalItems: action.payload.headers['x-total-count'],
          entities: action.payload.data
        }
      };
    case SUCCESS(TIME_ENTRY_ACTION_TYPES.FETCH_TIMEENTRY_LIST):
      return {
        ...state,
        loading: false,
        timeEntries: {
          totalItems: action.payload.headers['x-total-count'],
          entities: action.payload.data
        }
      };
    case SUCCESS(ACTION_TYPES.START_PROJECT):
      const newTimeEntry = action.payload.data;
      const newEntities = [...state.timeEntries.entities, newTimeEntry];
      return {
        ...state,
        timeEntries: {
          entities: newEntities,
          totalItems: newEntities.length
        },
        userDetails: {
          activeTimeEntry: newTimeEntry
        }
      };
    case SUCCESS(ACTION_TYPES.STOP_PROJECT):
      return {
        ...state,
        userDetails: {
          activeTimeEntry: null
        }
      };
    case SUCCESS(ACTION_TYPES.USER_EXTRAS):
      return {
        ...state,
        userDetails: action.payload.data
      };
    case ACTION_TYPES.SELECT_DATE:
      return {
        ...state,
        selectedDate: action.payload
      };
    default:
      return state;
  }
};

// Actions
export const startProject = (project: IProject) => ({
  type: ACTION_TYPES.START_PROJECT,
  payload: axios.post(`/api/worktajm/startProject/${project.id}`)
});

export const stopProject = (project: IProject) => ({
  type: ACTION_TYPES.STOP_PROJECT,
  payload: axios.post(`/api/worktajm/stopProject/${project.id}`)
});

export const loadUserDetails = () => ({
  type: ACTION_TYPES.USER_EXTRAS,
  payload: axios.get('/api/user-extras')
});

export const selectDate = (date: Date) => ({
  type: ACTION_TYPES.SELECT_DATE,
  payload: date
});
