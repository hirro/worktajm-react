import './reports.scss';

import * as React from 'react';
import { Link } from 'react-router-dom';
import { ICrudGetAllAction, Translate } from 'react-jhipster';
import { connect } from 'react-redux';

import { getSession } from 'app/shared/reducers/authentication';
import { Card, Button, CardHeader, CardFooter, CardBody, CardTitle, CardText } from 'reactstrap';
import { IProject } from 'app/shared/model/project.model';
import { getEntities as getProjectEntities } from 'app/entities/project/project.reducer';
import { getEntities as getTimeEntryEntities } from 'app/entities/time-entry/time-entry.reducer';
import { ITimeEntry } from 'app/shared/model/time-entry.model';

export interface IReportsProps {
  account: any;
  getSession: Function;
  projects: any[];
  timeEntries: ITimeEntry[];
  getProjectEntities: ICrudGetAllAction<IProject>;
  getTimeEntryEntities: ICrudGetAllAction<ITimeEntry>;
}

export interface IReportsState {
  currentUser: any;
  projects: IProject[];
}

export class Reports extends React.Component<IReportsProps, IReportsState> {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.getSession();
  }

  componentDidMount() {
    this.props.getProjectEntities();
    this.props.getTimeEntryEntities();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      currentUser: nextProps.account
    });
  }

  render() {
    const { projects, timeEntries } = this.props;
    return <div className="row" />;
  }
}

const mapStateToProps = ({ reports }) => ({
  userDetails: reports.userDetails,
  projects: reports.projects.entities,
  timeEntries: reports.timeEntries.entities
});

const mapDispatchToProps = {
  getSession,
  getProjectEntities,
  getTimeEntryEntities
};

export default connect(mapStateToProps, mapDispatchToProps)(Reports);
