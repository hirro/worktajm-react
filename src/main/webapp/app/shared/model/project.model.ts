import { IUser } from './user.model';
import { ICustomer } from 'app/shared/model//customer.model';

export interface IProject {
  id?: number;
  name?: string;
  description?: string;
  hourlyRate?: number;
  members?: IUser[];
  customer?: ICustomer;
}

export const defaultValue: Readonly<IProject> = {};
