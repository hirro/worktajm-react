import { IProject } from 'app/shared/model//project.model';
import { IDomain } from 'app/shared/model//domain.model';

export interface ICustomer {
  id?: number;
  name?: string;
  projects?: IProject[];
  domain?: IDomain;
}

export const defaultValue: Readonly<ICustomer> = {};
