import { ICustomer } from 'app/shared/model//customer.model';
import { IUser } from './user.model';

export interface IDomain {
  id?: number;
  name?: string;
  customers?: ICustomer[];
  members?: IUser[];
}

export const defaultValue: Readonly<IDomain> = {};
