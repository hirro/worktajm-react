import { Moment } from 'moment';
import { IProject } from 'app/shared/model//project.model';
import { IUser } from './user.model';

export interface ITimeEntry {
  id?: number;
  start?: Moment;
  end?: Moment;
  comment?: string;
  project?: IProject;
  user?: IUser;
}

export const defaultValue: Readonly<ITimeEntry> = {};
