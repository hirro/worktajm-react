import React from 'react';
import { Card } from 'reactstrap';
import { Switch } from 'react-router-dom';

// tslint:disable-next-line:no-unused-variable
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Project from './project';
import Customer from './customer';
import TimeEntry from './time-entry';
import Domain from './domain';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div className="container-fluid view-container" id="app-view-container">
    <Card className="jh-card">
      <Switch>
        {/* prettier-ignore */}
        <ErrorBoundaryRoute path={`${match.url}/project`} component={Project} />
        <ErrorBoundaryRoute path={`${match.url}/customer`} component={Customer} />
        <ErrorBoundaryRoute path={`${match.url}/time-entry`} component={TimeEntry} />
        <ErrorBoundaryRoute path={`${match.url}/domain`} component={Domain} />
        {/* jhipster-needle-add-route-path - JHipster will routes here */}
      </Switch>
    </Card>
  </div>
);

export default Routes;
