import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './project.reducer';
import { IProject } from 'app/shared/model/project.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProjectDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export class ProjectDetail extends React.Component<IProjectDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { projectEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="worktajmApp.project.detail.title">Project</Translate> [<b>{projectEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="worktajmApp.project.name">Name</Translate>
              </span>
              <UncontrolledTooltip target="name">
                <Translate contentKey="worktajmApp.project.help.name" />
              </UncontrolledTooltip>
            </dt>
            <dd>{projectEntity.name}</dd>
            <dt>
              <span id="description">
                <Translate contentKey="worktajmApp.project.description">Description</Translate>
              </span>
              <UncontrolledTooltip target="description">
                <Translate contentKey="worktajmApp.project.help.description" />
              </UncontrolledTooltip>
            </dt>
            <dd>{projectEntity.description}</dd>
            <dt>
              <span id="hourlyRate">
                <Translate contentKey="worktajmApp.project.hourlyRate">Hourly Rate</Translate>
              </span>
              <UncontrolledTooltip target="hourlyRate">
                <Translate contentKey="worktajmApp.project.help.hourlyRate" />
              </UncontrolledTooltip>
            </dt>
            <dd>{projectEntity.hourlyRate}</dd>
            <dt>
              <Translate contentKey="worktajmApp.project.members">Members</Translate>
            </dt>
            <dd>
              {projectEntity.members
                ? projectEntity.members.map((val, i) => (
                    <span key={val.id}>
                      <a>{val.email}</a>
                      {i === projectEntity.members.length - 1 ? '' : ', '}
                    </span>
                  ))
                : null}{' '}
            </dd>
            <dt>
              <Translate contentKey="worktajmApp.project.customer">Customer</Translate>
            </dt>
            <dd>{projectEntity.customer ? projectEntity.customer.name : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/project" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/project/${projectEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ project }: IRootState) => ({
  projectEntity: project.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectDetail);
