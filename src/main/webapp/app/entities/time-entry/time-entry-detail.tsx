import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './time-entry.reducer';
import { ITimeEntry } from 'app/shared/model/time-entry.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITimeEntryDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export class TimeEntryDetail extends React.Component<ITimeEntryDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { timeEntryEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="worktajmApp.timeEntry.detail.title">TimeEntry</Translate> [<b>{timeEntryEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="start">
                <Translate contentKey="worktajmApp.timeEntry.start">Start</Translate>
              </span>
              <UncontrolledTooltip target="start">
                <Translate contentKey="worktajmApp.timeEntry.help.start" />
              </UncontrolledTooltip>
            </dt>
            <dd>
              <TextFormat value={timeEntryEntity.start} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="end">
                <Translate contentKey="worktajmApp.timeEntry.end">End</Translate>
              </span>
              <UncontrolledTooltip target="end">
                <Translate contentKey="worktajmApp.timeEntry.help.end" />
              </UncontrolledTooltip>
            </dt>
            <dd>
              <TextFormat value={timeEntryEntity.end} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="comment">
                <Translate contentKey="worktajmApp.timeEntry.comment">Comment</Translate>
              </span>
              <UncontrolledTooltip target="comment">
                <Translate contentKey="worktajmApp.timeEntry.help.comment" />
              </UncontrolledTooltip>
            </dt>
            <dd>{timeEntryEntity.comment}</dd>
            <dt>
              <Translate contentKey="worktajmApp.timeEntry.project">Project</Translate>
            </dt>
            <dd>{timeEntryEntity.project ? timeEntryEntity.project.name : ''}</dd>
            <dt>
              <Translate contentKey="worktajmApp.timeEntry.user">User</Translate>
            </dt>
            <dd>{timeEntryEntity.user ? timeEntryEntity.user.email : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/time-entry" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/time-entry/${timeEntryEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ timeEntry }: IRootState) => ({
  timeEntryEntity: timeEntry.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TimeEntryDetail);
