import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProject } from 'app/shared/model/project.model';
import { getEntities as getProjects } from 'app/entities/project/project.reducer';
import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { getEntity, updateEntity, createEntity, reset } from './time-entry.reducer';
import { ITimeEntry } from 'app/shared/model/time-entry.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITimeEntryUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export interface ITimeEntryUpdateState {
  isNew: boolean;
  projectId: number;
  userId: number;
}

export class TimeEntryUpdate extends React.Component<ITimeEntryUpdateProps, ITimeEntryUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      projectId: 0,
      userId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProjects();
    this.props.getUsers();
  }

  saveEntity = (event, errors, values) => {
    values.start = new Date(values.start);
    values.end = new Date(values.end);

    if (errors.length === 0) {
      const { timeEntryEntity } = this.props;
      const entity = {
        ...timeEntryEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/time-entry');
  };

  render() {
    const { timeEntryEntity, projects, users, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="worktajmApp.timeEntry.home.createOrEditLabel">
              <Translate contentKey="worktajmApp.timeEntry.home.createOrEditLabel">Create or edit a TimeEntry</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : timeEntryEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="time-entry-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="startLabel" for="start">
                    <Translate contentKey="worktajmApp.timeEntry.start">Start</Translate>
                  </Label>
                  <AvInput
                    id="time-entry-start"
                    type="datetime-local"
                    className="form-control"
                    name="start"
                    value={isNew ? null : convertDateTimeFromServer(this.props.timeEntryEntity.start)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                  <UncontrolledTooltip target="startLabel">
                    <Translate contentKey="worktajmApp.timeEntry.help.start" />
                  </UncontrolledTooltip>
                </AvGroup>
                <AvGroup>
                  <Label id="endLabel" for="end">
                    <Translate contentKey="worktajmApp.timeEntry.end">End</Translate>
                  </Label>
                  <AvInput
                    id="time-entry-end"
                    type="datetime-local"
                    className="form-control"
                    name="end"
                    value={isNew ? null : convertDateTimeFromServer(this.props.timeEntryEntity.end)}
                  />
                  <UncontrolledTooltip target="endLabel">
                    <Translate contentKey="worktajmApp.timeEntry.help.end" />
                  </UncontrolledTooltip>
                </AvGroup>
                <AvGroup>
                  <Label id="commentLabel" for="comment">
                    <Translate contentKey="worktajmApp.timeEntry.comment">Comment</Translate>
                  </Label>
                  <AvField id="time-entry-comment" type="text" name="comment" />
                  <UncontrolledTooltip target="commentLabel">
                    <Translate contentKey="worktajmApp.timeEntry.help.comment" />
                  </UncontrolledTooltip>
                </AvGroup>
                <AvGroup>
                  <Label for="project.name">
                    <Translate contentKey="worktajmApp.timeEntry.project">Project</Translate>
                  </Label>
                  <AvInput
                    id="time-entry-project"
                    type="select"
                    className="form-control"
                    name="project.id"
                    value={isNew ? projects[0] && projects[0].id : timeEntryEntity.project.id}
                  >
                    {projects
                      ? projects.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="user.email">
                    <Translate contentKey="worktajmApp.timeEntry.user">User</Translate>
                  </Label>
                  <AvInput
                    id="time-entry-user"
                    type="select"
                    className="form-control"
                    name="user.id"
                    value={isNew ? users[0] && users[0].id : timeEntryEntity.user.id}
                  >
                    {users
                      ? users.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.email}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/time-entry" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  projects: storeState.project.entities,
  users: storeState.userManagement.users,
  timeEntryEntity: storeState.timeEntry.entity,
  loading: storeState.timeEntry.loading,
  updating: storeState.timeEntry.updating
});

const mapDispatchToProps = {
  getProjects,
  getUsers,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TimeEntryUpdate);
