package com.arnellconsulting.worktajm.service;

import com.arnellconsulting.worktajm.domain.Customer;
import com.arnellconsulting.worktajm.domain.Domain;
import com.arnellconsulting.worktajm.domain.User;
import com.arnellconsulting.worktajm.repository.CustomerRepository;
import com.arnellconsulting.worktajm.repository.search.CustomerSearchRepository;
import com.arnellconsulting.worktajm.web.rest.errors.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Customer.
 */
@Service
@Transactional
public class CustomerService {

    private final Logger log = LoggerFactory.getLogger(CustomerService.class);

    private UserService userService;

    private final CustomerRepository customerRepository;

    private final CustomerSearchRepository customerSearchRepository;

    private final DomainService domainService;

    public CustomerService(UserService userService,
                           CustomerRepository customerRepository,
                           CustomerSearchRepository customerSearchRepository,
                           DomainService domainService) {
        this.userService = userService;
        this.customerRepository = customerRepository;
        this.customerSearchRepository = customerSearchRepository;
        this.domainService = domainService;
    }

    /**
     * Save a customer.
     *
     * @param customer the entity to save
     * @return the persisted entity
     */
    public Customer save(Customer customer) {

        if (customer == null) {
            throw new BadRequestAlertException(ErrorConstants.ERR_VALIDATION, "customer", "required");
        }
        if (customer.getDomain() == null) {
            throw new BadRequestAlertException(ErrorConstants.ERR_VALIDATION, "customer.domain", "required");
        }

        User user = userService.getLoggedInUser();

        // Check domain is authorized
        List<Domain> memberOfDomains = domainService.findDomainsUserIsMemberOf(user);
        if (!memberOfDomains.stream().map(Domain::getId).collect(Collectors.toList()).contains(customer.getDomain().getId())) {
            throw new NotMemberException();
        }

        if (customer.getId() == null) {
            log.debug("Request to create Customer : {}", customer);
        } else {
            log.debug("Request to update Customer : {}", customer);

            // Check user may perform this operation: User must be a member of the domain managing the customer
            Optional<Customer> domainInDb = customerRepository.findById(customer.getId());
            if (!domainInDb.isPresent()) {
                throw new EntityNotFoundException();
            }

            // Check current user is member
            Domain domain = domainInDb.get().getDomain();
            if ((domain == null)
                || (!domain.isMember(user.getId()))) {
                throw new NotMemberException();
            }
        }
        Customer result = customerRepository.save(customer);
        customerSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the customers as pages
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Customer> findAll(Pageable pageable, User user) {
        log.debug("Request to get all Customers");
        List<Domain> memberOfDomains = domainService.findDomainsUserIsMemberOf(user);
        return customerRepository.findAllByDomainIn(pageable, memberOfDomains);
    }


    /**
     * Get all the customers as list
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Customer> findAll(User user) {
        log.debug("Request to get all Customers");
        List<Domain> memberOfDomains = domainService.findDomainsUserIsMemberOf(user);
        return customerRepository.findAllByDomainIn(memberOfDomains);
    }

    /**
     * Get one customer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Customer> findOne(Long id, User user) {
        log.debug("Request to get Customer : {}", id);

        Optional<Customer> customer = customerRepository.findById(id);
        if (customer == null) {
            throw new EntityNotFoundException();
        }

        // Check user may read the customer
        if (!customer.get().getDomain().isMember(user.getId())) {
            throw new NotMemberException();
        }

        return customer;
    }

    /**
     * Delete the customer by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Customer : {}", id);

        Optional<Customer> customer = customerRepository.findById(id);
        if (customer.isPresent()) {
            checkAccessToCustomer(customer.get());
            customerRepository.deleteById(id);
            customerSearchRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    /**
     * Search for the customer corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Customer> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Customers for query {}", query);
        return customerSearchRepository.search(queryStringQuery(query), pageable);
    }

    private void checkAccessToCustomer(Customer customer) {

        // Must find user
        Optional<User> user = this.userService.getUserWithAuthorities();
        if (!user.isPresent()) {
            throw new LoginRequiredException();
        }

        // Make sure user has access to domain
        if (!customer.getDomain().isMember(user.get().getId())) {
            throw new NotMemberException();
        }
    }
}
