package com.arnellconsulting.worktajm.service;

import com.arnellconsulting.worktajm.domain.Customer;
import com.arnellconsulting.worktajm.domain.Domain;
import com.arnellconsulting.worktajm.domain.Project;
import com.arnellconsulting.worktajm.domain.User;
import com.arnellconsulting.worktajm.repository.DomainRepository;
import com.arnellconsulting.worktajm.repository.search.DomainSearchRepository;
import com.arnellconsulting.worktajm.web.rest.errors.EntityNotFoundException;
import com.arnellconsulting.worktajm.web.rest.errors.NotMemberException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Domain.
 */
@Service
@Transactional
public class DomainService {

    private final Logger log = LoggerFactory.getLogger(DomainService.class);

    private UserService userService;

    private final DomainRepository domainRepository;

    private final DomainSearchRepository domainSearchRepository;

    public DomainService(UserService userService, DomainRepository domainRepository, DomainSearchRepository domainSearchRepository) {
        this.userService = userService;
        this.domainRepository = domainRepository;
        this.domainSearchRepository = domainSearchRepository;
    }

    /**
     * Save a domain.
     *
     * @param domain the entity to save
     * @return the persisted entity
     */
    public Domain save(Domain domain) {
        log.debug("Request to save Domain : {}", domain);

        User user = userService.getLoggedInUser();
        if (domain.getId() == null) {
            // Fix relationships on creation
            for (Customer c : domain.getCustomers()) {
                c.setDomain(domain);
                for (Project p : c.getProjects()) {
                    p.setCustomer(c);
                    p.addMembers(user);
                }
            }
        } else {

            // Get eagerly loaded domain
            Optional<Domain> domainInDb = domainRepository.findOneWithEagerRelationships(domain.getId());
            if (!domainInDb.isPresent()) {
                throw new EntityNotFoundException();
            }

            // Check current user is member
            if (!domainInDb.get().isMember(user.getId())) {
                throw new NotMemberException();
            }
        }

        // Add current user as authorized
        domain.addMembers(user);

        // Persist
        Domain result = domainRepository.save(domain);
        domainSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the domains (that user is member of).
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Domain> findAll(Pageable pageable) {
        log.debug("Request to get all Domains");
        User user = userService.getLoggedInUser();
        return domainRepository.findAllByMembersIs(user, pageable);
    }

    /**
     * Get all the domains (that user is member of).
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Domain> findDomainsUserIsMemberOf(User user) {
        log.debug("Request to get all Domains");
        return domainRepository.findAllByMembersIs(user);
    }

    /**
     * Get all the Domain with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<Domain> findAllWithEagerRelationships(Pageable pageable) {
        User user = userService.getLoggedInUser();
        return domainRepository.findAllWithEagerRelationships(pageable, user);
    }


    /**
     * Get one domain by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Domain> findOne(Long id) {
        log.debug("Request to get Domain : {}", id);
        User user = userService.getLoggedInUser();
        Optional<Domain> domain = domainRepository.findById(id);
        if (domain.isPresent()) {
            checkAccessToDomain(domain.get());

            // Ugly force fetch of lazy resource
            domain.get().getMembers().stream().count();
            domain.get().getCustomers().stream().count();

            return domain;
        }
        throw new EntityNotFoundException();
    }

    /**
     * Delete the domain by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Domain : {}", id);
        Optional<Domain> domain = domainRepository.findById(id);
        if (domain.isPresent()) {
            checkAccessToDomain(domain.get());
            domainRepository.deleteById(id);
            domainSearchRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    /**
     * Search for the domain corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Domain> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Domains for query {}", query);
        return domainSearchRepository.search(queryStringQuery(query), pageable);
    }

    private void checkAccessToDomain(Domain domain) {

        // Must find user
        Optional<User> user = this.userService.getUserWithAuthorities();
        if (!user.isPresent()) {
            throw new AccessDeniedException("User not found");
        }

        // Make sure user has access to domain
        if (!domain.isMember(user.get().getId())) {
            throw new AccessDeniedException("User is not allowed to access domain");
        }
    }

}
