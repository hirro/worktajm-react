package com.arnellconsulting.worktajm.service;

import com.arnellconsulting.worktajm.domain.Customer;
import com.arnellconsulting.worktajm.domain.Domain;
import com.arnellconsulting.worktajm.domain.Project;
import com.arnellconsulting.worktajm.domain.User;
import com.arnellconsulting.worktajm.repository.ProjectRepository;
import com.arnellconsulting.worktajm.repository.search.ProjectSearchRepository;
import com.arnellconsulting.worktajm.web.rest.errors.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Project.
 */
@Service
@Transactional
public class ProjectService {

    private final Logger log = LoggerFactory.getLogger(ProjectService.class);

    private final ProjectRepository projectRepository;

    private final ProjectSearchRepository projectSearchRepository;

    private final UserService userService;

    private final CustomerService customerService;

    public ProjectService(ProjectRepository projectRepository,
                          ProjectSearchRepository projectSearchRepository,
                          UserService userService,
                          CustomerService customerService) {
        this.projectRepository = projectRepository;
        this.projectSearchRepository = projectSearchRepository;
        this.userService = userService;
        this.customerService = customerService;
    }

    /**
     * Save a project.
     *
     * @param project the entity to save
     * @return the persisted entity
     */
    public Project save(Project project, User user) {

        if (project == null) {
            throw new BadRequestAlertException(ErrorConstants.ERR_VALIDATION, "project", "required");
        }
        if (project.getCustomer() == null) {
            throw new BadRequestAlertException(ErrorConstants.ERR_VALIDATION, "project.customer", "required");
        }

        if (project.getId() == null) {
            log.debug("Request to create Project : {}", project);
        } else {
            log.debug("Request to update Project : {}", project);
        }

        // Check customer is authorized
        List<Customer> authorizedCustomers = customerService.findAll(user);
        if (!authorizedCustomers.stream().map(Customer::getId).collect(Collectors.toList()).contains(project.getCustomer().getId())) {
            throw new NotMemberException();
        }

        Project result = projectRepository.save(project);
        projectSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the projects.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Project> findAll(Pageable pageable, User user) {
        log.debug("Request to get all Projects");

        // Get all customers user has access to
        List<Customer> customers = customerService.findAll(user);
        return projectRepository.findAllByCustomerInAndMembersContains(pageable, customers, user);
    }

    /**
     * Get all the Project with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<Project> findAllWithEagerRelationships(Pageable pageable, User user) {

        // Get all customers user has access to
        List<Customer> customers = customerService.findAll(user);
        throw new IllegalArgumentException("");
//        return projectRepository.findAllWithEagerRelationships(pageable);
    }


    /**
     * Get one project by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Project> findOne(Long id, User user) {
        log.debug("Request to get Project : {}", id);
        Optional<Project> project =  projectRepository.findOneWithEagerRelationships(id);
        if (project == null) {
            throw new EntityNotFoundException();
        }

        // Check user may read the customer
        checkAccess(project.get(), user);

        return project;
    }

    /**
     * Delete the project by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id, User user) {
        log.debug("Request to delete Project : {}", id);
        Optional<Project> project = projectRepository.findById(id);
        if (!project.isPresent()) {
            throw new EntityNotFoundException();
        }

        checkAccess(project.get(), user);

        projectRepository.deleteById(id);
        projectSearchRepository.deleteById(id);
    }

    /**
     * Search for the project corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Project> search(String query, Pageable pageable, User user) {
        log.debug("Request to search for a page of Projects for query {}", query);
        return projectSearchRepository.search(
            boolQuery()
                .must(termQuery("user.id", user.getId()))
                .must(queryStringQuery(query)),
            pageable);
    }

    private void checkAccess(Project project, User user) {
        if (project == null) {
            throw new BadRequestAlertException(ErrorConstants.ERR_VALIDATION, "checkAccess", "project must not be NULL");
        }
        if (project.getCustomer() == null) {
            throw new BadRequestAlertException(ErrorConstants.ERR_VALIDATION, "checkAccess", "projec.customer must not be NULL");
        }

        // Make sure user has access to domain
        List<Long> validCustomerIds = customerService.findAll(user).stream().map(Customer::getId).collect(Collectors.toList());
        Long customerId = project.getCustomer().getId();
        if (!validCustomerIds.contains(customerId)) {
            throw new NotMemberException();
        }
    }

}
