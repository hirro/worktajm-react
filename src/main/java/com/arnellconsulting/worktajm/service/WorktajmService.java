package com.arnellconsulting.worktajm.service;

import com.arnellconsulting.worktajm.domain.TimeEntry;
import com.arnellconsulting.worktajm.domain.User;
import com.arnellconsulting.worktajm.repository.TimeEntryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
@Transactional
public class WorktajmService {

    private final Logger log = LoggerFactory.getLogger(WorktajmService.class);

    private final TimeEntryRepository timeEntryRepository;

    public WorktajmService(TimeEntryRepository timeEntryRepository) {
        this.timeEntryRepository = timeEntryRepository;
    }

    /**
     * Get all the timeEntries.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TimeEntry> findAllInTimePeriod(Pageable pageable,
                                               User user,
                                               Optional<ZonedDateTime> from,
                                               Optional<ZonedDateTime> to) {
        ZonedDateTime searchFrom;
        if (from.isPresent()) {
            // Search from start of day (time is cleared)
            searchFrom = from.get().truncatedTo(ChronoUnit.DAYS);
        } else {
            // No start date provided, search from start of of time
            searchFrom = ZonedDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC);
        }

        ZonedDateTime searchTo;
        if (to.isPresent()) {
            // Search to end of day (time is cleared)
            searchTo = to.get().truncatedTo(ChronoUnit.DAYS).plusDays(1).minusNanos(1);
        } else {
            // No end date provided
            searchTo = ZonedDateTime.now().plusDays(1);
        }

        log.debug("findAllInTimePeriod between {} and {}", searchFrom, searchTo);
        return timeEntryRepository.findAllByUserAndStartBetweenOrderByStart(pageable, user, searchFrom, searchTo);
    }
}
