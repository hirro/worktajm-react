package com.arnellconsulting.worktajm.service;

import com.arnellconsulting.worktajm.domain.TimeEntry;
import com.arnellconsulting.worktajm.domain.User;
import com.arnellconsulting.worktajm.repository.TimeEntryRepository;
import com.arnellconsulting.worktajm.repository.search.TimeEntrySearchRepository;
import com.arnellconsulting.worktajm.web.rest.errors.BadRequestAlertException;
import com.arnellconsulting.worktajm.web.rest.errors.EntityNotFoundException;
import com.arnellconsulting.worktajm.web.rest.errors.ErrorConstants;
import com.arnellconsulting.worktajm.web.rest.errors.NotMemberException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TimeEntry.
 */
@Service
@Transactional
public class TimeEntryService {

    private final Logger log = LoggerFactory.getLogger(TimeEntryService.class);

    private final TimeEntryRepository timeEntryRepository;

    private final TimeEntrySearchRepository timeEntrySearchRepository;

    public TimeEntryService(TimeEntryRepository timeEntryRepository, TimeEntrySearchRepository timeEntrySearchRepository) {
        this.timeEntryRepository = timeEntryRepository;
        this.timeEntrySearchRepository = timeEntrySearchRepository;
    }

    /**
     * Save a timeEntry.
     *
     * @param timeEntry the entity to save
     * @return the persisted entity
     */
    public TimeEntry save(TimeEntry timeEntry, User user) {

        if (timeEntry == null) {
            throw new BadRequestAlertException(ErrorConstants.ERR_VALIDATION, "project", "required");
        }
        else if (timeEntry.getId() == null) {
            log.debug("Request to create TimeEntry : {}", timeEntry);
            timeEntry.setUser(user);
        } else {
            log.debug("Request to save TimeEntry : {}", timeEntry);

        }

        checkAccess(timeEntry, user);

        // Persists
        TimeEntry result = timeEntryRepository.save(timeEntry);
        timeEntrySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the timeEntries.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TimeEntry> findAll(Pageable pageable, User user) {
        log.debug("Request to get all TimeEntries");
        return timeEntryRepository.findAllByUser(pageable, user);
    }

    /**
     * Get one timeEntry by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<TimeEntry> findOne(Long id, User user) {
        log.debug("Request to get TimeEntry : {}", id);
        Optional<TimeEntry> timeEntry = timeEntryRepository.findById(id);
        if (!timeEntry.isPresent()) {
            throw new EntityNotFoundException();
        }
        checkAccess(timeEntry.get(), user);
        return timeEntry;
    }

    /**
     * Delete the timeEntry by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id, User user) {
        log.debug("Request to delete TimeEntry : {}", id);

        Optional<TimeEntry> timeEntry = timeEntryRepository.findById(id);
        if (!timeEntry.isPresent()) {
            throw new EntityNotFoundException();
        }

        checkAccess(timeEntry.get(), user);

        timeEntryRepository.deleteById(id);
        timeEntrySearchRepository.deleteById(id);
    }

    /**
     * Search for the timeEntry corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TimeEntry> search(String query, Pageable pageable, User user) {
        log.debug("Request to search for a page of TimeEntries for query {}", query);
        return timeEntrySearchRepository.search(queryStringQuery(query), pageable);
    }

    private void checkAccess(TimeEntry timeEntry, User user) {
        if ((timeEntry.getUser() == null) ||
            (timeEntry.getUser().getId() != user.getId())) {
            throw new BadRequestAlertException(
                ErrorConstants.UNAUTHORIZED,
                "checkAccess",
                "timeEntry",
                "May only modify time entries owned by users");
        }
    }

}
