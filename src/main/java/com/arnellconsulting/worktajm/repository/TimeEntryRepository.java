package com.arnellconsulting.worktajm.repository;

import com.arnellconsulting.worktajm.domain.TimeEntry;
import com.arnellconsulting.worktajm.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the TimeEntry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TimeEntryRepository extends JpaRepository<TimeEntry, Long> {

    @Query("select time_entry from TimeEntry time_entry where time_entry.user.login = ?#{principal.username}")
    List<TimeEntry> findByUserIsCurrentUser();


    Page<TimeEntry> findAllByUser(Pageable pageable, User user);
    Page<TimeEntry> findAllByUserAndStartBetweenOrderByStart(Pageable pageable, User user, ZonedDateTime after, ZonedDateTime before);

    Optional<TimeEntry> findByIdAndUser(Long id, User user);
}
