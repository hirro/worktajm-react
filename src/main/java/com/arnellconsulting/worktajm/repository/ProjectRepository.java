package com.arnellconsulting.worktajm.repository;

import com.arnellconsulting.worktajm.domain.Customer;
import com.arnellconsulting.worktajm.domain.Project;
import com.arnellconsulting.worktajm.domain.User;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Project entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query(value = "select distinct project from Project project left join fetch project.members",
        countQuery = "select count(distinct project) from Project project")
    Page<Project> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct project from Project project left join fetch project.members")
    List<Project> findAllWithEagerRelationships();

    @Query("select project from Project project left join fetch project.members where project.id =:id")
    Optional<Project> findOneWithEagerRelationships(@Param("id") Long id);

    Page<Project> findAllByCustomerIn(Pageable pageable, List<Customer> customers);

    Page<Project> findAllByCustomerInAndMembersContains(Pageable pageable, List<Customer> customers, User user);
}
