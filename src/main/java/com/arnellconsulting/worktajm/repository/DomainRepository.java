package com.arnellconsulting.worktajm.repository;

import com.arnellconsulting.worktajm.domain.Domain;
import com.arnellconsulting.worktajm.domain.User;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Domain entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DomainRepository extends JpaRepository<Domain, Long> {

    @Query(value = "select distinct domain from Domain domain left join fetch domain.members where :user in (domain.members)",
        countQuery = "select count(distinct domain) from Domain domain")
    Page<Domain> findAllWithEagerRelationships(Pageable pageable, User user);

    @Query(value = "select distinct domain from Domain domain left join fetch domain.members where :user in (domain.members)")
    List<Domain> findAllWithEagerRelationships(User user);

    @Query("select domain from Domain domain left join fetch domain.members where domain.id =:id")
    Optional<Domain> findOneWithEagerRelationships(@Param("id") Long id);

    /**
     * Find all domains permitted for user.
     * @param user
     * @param pageable
     * @return
     */
    Page<Domain> findAllByMembersIs(User user, Pageable pageable);
    List<Domain> findAllByMembersIs(User user);

    void deleteByIdAndMembersIn(Long id, User user);
}
