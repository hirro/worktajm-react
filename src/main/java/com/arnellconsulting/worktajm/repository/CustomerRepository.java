package com.arnellconsulting.worktajm.repository;

import com.arnellconsulting.worktajm.domain.Customer;
import com.arnellconsulting.worktajm.domain.Domain;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Customer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Page<Customer> findAllByDomainIn(Pageable pageable, List<Domain> domains);
    List<Customer> findAllByDomainIn(List<Domain> domains);
}
