package com.arnellconsulting.worktajm.web.rest;

import com.arnellconsulting.worktajm.domain.User;
import com.arnellconsulting.worktajm.repository.UserRepository;
import com.arnellconsulting.worktajm.security.SecurityUtils;
import com.codahale.metrics.annotation.Timed;
import com.arnellconsulting.worktajm.domain.UserExtra;

import com.arnellconsulting.worktajm.repository.UserExtraRepository;
import com.arnellconsulting.worktajm.repository.search.UserExtraSearchRepository;
import com.arnellconsulting.worktajm.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * REST controller for managing UserExtra.
 */
@RestController
@RequestMapping("/api")
public class UserExtraResource {

    private final Logger log = LoggerFactory.getLogger(UserExtraResource.class);

    private static final String ENTITY_NAME = "userExtra";

    private final UserRepository userRepository;

    private final UserExtraRepository userExtraRepository;

    private final UserExtraSearchRepository userExtraSearchRepository;

    public UserExtraResource(UserRepository userRepository, UserExtraRepository userExtraRepository, UserExtraSearchRepository userExtraSearchRepository) {
        this.userRepository = userRepository;
        this.userExtraRepository = userExtraRepository;
        this.userExtraSearchRepository = userExtraSearchRepository;
    }

    /**
     * PUT  /user-extras : Updates an existing userExtra.
     *
     * @param userExtra the userExtraDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userExtraDTO,
     * or with status 400 (Bad Request) if the userExtraDTO is not valid,
     * or with status 500 (Internal Server Error) if the userExtraDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-extras")
    @Timed
    public ResponseEntity<UserExtra> updateUserExtra(@RequestBody UserExtra userExtra) throws URISyntaxException {
        log.debug("REST request to update UserExtra : {}", userExtra);
        UserExtra result;

        // Check: Must have id
        if (userExtra.getId() == null) {
            log.warn("Tried to update user extra with missing id");
            return ResponseEntity.notFound().build();
        }

        // Check: Must be logged in
        Optional<User> loggedInUser = getLoggedInUser();
        if (!loggedInUser.isPresent()) {
            log.warn("Attempted to update user extra while not being logged in");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        // Check: Must have user extra
        Optional<UserExtra> optionalUserExtraInDb = userExtraRepository.findById(userExtra.getId());
        if (!optionalUserExtraInDb.isPresent()) {
            log.warn("Attempted to update user extra while not being logged in");
            return ResponseEntity.notFound().build();
        }
        UserExtra userExtraInDb = optionalUserExtraInDb.get();

        // Check: May only modify user extra owned by itself
        if (userExtraInDb.getId() != userExtraInDb.getId()) {
            log.warn("Tried is modify user extra for other user. Violating user: {}", loggedInUser.get().getLogin());
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        try {
            // Copy new values from received object to database object
            userExtraInDb.copyFrom(userExtra);
            result = userExtraRepository.save(userExtraInDb);
            userExtraSearchRepository.save(userExtraInDb);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * GET  /user-extras/
     *
     * @return the ResponseEntity with status 200 (OK) and with body the userExtraDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-extras")
    @Timed
    public ResponseEntity<UserExtra> getUserExtra() {
        log.debug("REST request to get UserExtra");
        try {
            Optional<User> user = getLoggedInUser();
            Optional<UserExtra> userExtraInDb = userExtraRepository.findUserExtraByUser(user.get());
            return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userExtraInDb.get()));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

    }

    private Optional<User> getLoggedInUser() {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        if (userLogin.isPresent()) {
            String resolvedLogin = userLogin.get().toLowerCase();
            return userRepository.findOneByLogin(resolvedLogin);
        }
        return Optional.empty();
    }
}
