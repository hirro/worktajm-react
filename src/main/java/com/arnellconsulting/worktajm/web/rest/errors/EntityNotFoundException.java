package com.arnellconsulting.worktajm.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class EntityNotFoundException extends AbstractThrowableProblem {
    public EntityNotFoundException() {
        super(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "Entity does not exist", Status.NOT_FOUND);
    }
}
