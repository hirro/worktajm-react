package com.arnellconsulting.worktajm.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class NotMemberException extends AbstractThrowableProblem {
    public NotMemberException() {
        super(ErrorConstants.UNAUTHORIZED, "User must be member of domain", Status.UNAUTHORIZED);
    }
}
