package com.arnellconsulting.worktajm.web.rest;

import com.arnellconsulting.worktajm.domain.Project;
import com.arnellconsulting.worktajm.domain.TimeEntry;
import com.arnellconsulting.worktajm.domain.User;
import com.arnellconsulting.worktajm.domain.UserExtra;
import com.arnellconsulting.worktajm.repository.ProjectRepository;
import com.arnellconsulting.worktajm.repository.TimeEntryRepository;
import com.arnellconsulting.worktajm.repository.UserExtraRepository;
import com.arnellconsulting.worktajm.repository.UserRepository;
import com.arnellconsulting.worktajm.repository.search.ProjectSearchRepository;
import com.arnellconsulting.worktajm.security.SecurityUtils;
import com.arnellconsulting.worktajm.service.ProjectService;
import com.arnellconsulting.worktajm.service.UserService;
import com.arnellconsulting.worktajm.service.WorktajmService;
import com.arnellconsulting.worktajm.web.rest.errors.BadRequestAlertException;
import com.arnellconsulting.worktajm.web.rest.errors.InternalServerErrorException;
import com.arnellconsulting.worktajm.web.rest.util.HeaderUtil;
import com.arnellconsulting.worktajm.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * REST controller for managing Project.
 */
@RestController
@RequestMapping("/api/worktajm")
@Transactional
public class WorktajmResource {

    private final Logger log = LoggerFactory.getLogger(ProjectResource.class);

    private static final String ENTITY_NAME = "worktajm";

    private final ProjectRepository projectRepository;

    private final UserRepository userRepository;

    private final ProjectSearchRepository projectSearchRepository;

    private final UserExtraRepository userExtraRepository;

    private final TimeEntryRepository timeEntryRepository;

    private final ProjectService projectService;

    private final WorktajmService worktajmService;

    private final UserService userService;

    public WorktajmResource(ProjectRepository projectRepository,
                            UserRepository userRepository,
                            ProjectSearchRepository projectSearchRepository,
                            UserExtraRepository userExtraRepository,
                            TimeEntryRepository timeEntryRepository,
                            ProjectService projectService,
                            WorktajmService worktajmService,
                            UserService userService) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
        this.projectSearchRepository = projectSearchRepository;
        this.userExtraRepository = userExtraRepository;
        this.timeEntryRepository = timeEntryRepository;
        this.projectService = projectService;
        this.worktajmService = worktajmService;
        this.userService = userService;
    }

    /**
     * POST  /stopProject: Stops the project/
     *
     * @param id the project id to stop
     * @return the ResponseEntity with status 201 (Created) and with body the new TimeEntryDTO.
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stopProject/{id}")
    @Timed
    public ResponseEntity<TimeEntry> stopProject(@PathVariable Long id) throws URISyntaxException {
        log.debug("REST request to stop Project : {}", id);

        User user = getLoggedInUser();
        if (user == null) {
            throw new InternalServerErrorException("Not logged in");
        }

        // Check project exists
        Optional<Project> projectOptional = projectRepository.findOneWithEagerRelationships(id);
        if (!projectOptional.isPresent()) {
            throw new BadRequestAlertException("Bad request", ENTITY_NAME, "no such project id");
        }
        Project project = projectOptional.get();

        // Make sure user is member of project
        List<Long> userIdsForProject = project.getMembers().stream().map(User::getId).collect(Collectors.toList());
        if (!userIdsForProject.contains(user.getId())) {
            throw new BadRequestAlertException("Bad request", ENTITY_NAME, "no authorized");
        }

        // Make sure timer is running
        Optional<UserExtra> userExtraOptional = userExtraRepository.findById(user.getId());
        if (!userExtraOptional.isPresent()) {
            throw new BadRequestAlertException("Bad request", ENTITY_NAME, "No running time entry for user");
        }
        UserExtra userExtra = userExtraOptional.get();

        // Stop the running time entry and clear active timer from user extra.
        TimeEntry result;
        if (userExtra.getActiveTimeEntry() == null) {
            result = null;
        } else {
            TimeEntry timeEntry = userExtra.getActiveTimeEntry();
            timeEntry.setEnd(ZonedDateTime.now());
            userExtra.setActiveTimeEntry(null);
            userExtraRepository.save(userExtra);
            timeEntryRepository.save(timeEntry);
            result = timeEntry;
            projectSearchRepository.save(project);
        }

        // Return result
        return ResponseEntity.created(new URI("/api/worktajm/stopProject/" + id))
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id.toString()))
            .body(result);
    }

    /**
     * POST  /stopProject: Stops the project/
     *
     * @param id the project id to stop
     * @return the ResponseEntity with status 201 (Created) and with body the new TimeEntryDTO.
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/startProject/{id}")
    @Timed
    public ResponseEntity<TimeEntry> startProject(@PathVariable Long id) throws URISyntaxException {
        log.debug("REST request to start Project : {}", id);

        User user = getLoggedInUser();
        if (user == null) {
            throw new InternalServerErrorException("Not logged in");
        }

        // Check project exists
        Optional<Project> projectOptional = projectRepository.findById(id);
        if (!projectOptional.isPresent()) {
            throw new BadRequestAlertException("Bad request", ENTITY_NAME, "no such project id");
        }
        Project project = projectOptional.get();

        // Make sure user is member of project
        List<Long> userIdsForProject = project.getMembers().stream().map(User::getId).collect(Collectors.toList());
        if (!userIdsForProject.contains(user.getId())) {
            throw new BadRequestAlertException("Bad request", ENTITY_NAME, "no authorized");
        }

        // Make sure timer is not running already
        final UserExtra userExtra;
        Optional<UserExtra> userExtraOptional = userExtraRepository.findById(user.getId());
        if (!userExtraOptional.isPresent()) {
            userExtra = new UserExtra();
            userExtra.setUser(user);
        } else {
            userExtra = userExtraOptional.get();
            // Stop current timer
            if (userExtra.getActiveTimeEntry() != null) {
                userExtra.getActiveTimeEntry().setEnd(ZonedDateTime.now());
                timeEntryRepository.save(userExtra.getActiveTimeEntry());
                userExtra.setActiveTimeEntry(null);
                userExtraRepository.save(userExtra);
            }
        }

        // Start the running time entry and clear active timer from user extra.
        TimeEntry timeEntry = new TimeEntry();
        timeEntry.setUser(user);
        timeEntry.setStart(ZonedDateTime.now());
        timeEntry.setProject(project);
        userExtra.setActiveTimeEntry(timeEntry);
        userExtraRepository.save(userExtra);
        timeEntryRepository.save(timeEntry);
        TimeEntry result = timeEntry;
        projectSearchRepository.save(project);

        // Return result
        return ResponseEntity.created(new URI("/api/worktajm/stopProject/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * GET  /projects : get all the projects.
     *
     * @param pageable the pagination information
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of projects in body
     */
    @GetMapping("/projects")
    @Timed
    public ResponseEntity<List<Project>> findAllAuthorizedProjects(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Projects");
        User user = userService.getLoggedInUser();
        Page<Project> page = projectService.findAll(pageable, user);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/api/projects?eagerload=%b", eagerload));
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /time-entries : get all the timeEntries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of timeEntries in body
     */
    @GetMapping("/time-entries")
    @Timed
    public ResponseEntity<List<TimeEntry>> getTimeEntriesForTimePeriod(
        Pageable pageable,
        @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> from,
        @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> to)
    {
        log.debug("REST request to get a page of TimeEntries between {} and {}", from, to);
        Page<TimeEntry> page = worktajmService.findAllInTimePeriod(
            pageable,
            userService.getLoggedInUser(),
            from,
            to
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/time-entries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    private User getLoggedInUser() {
        Optional<String> userLogin = SecurityUtils.getCurrentUserLogin();
        return userRepository.findOneByLogin(userLogin.get()).get();
    }
}
