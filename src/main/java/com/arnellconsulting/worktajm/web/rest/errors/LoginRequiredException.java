package com.arnellconsulting.worktajm.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class LoginRequiredException extends AbstractThrowableProblem {
    public LoginRequiredException() {
        super(ErrorConstants.UNAUTHORIZED, "Login required", Status.UNAUTHORIZED);
    }
}
