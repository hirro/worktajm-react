#!/bin/bash

# Cleanup
rm -rf node_modules
rm yarn.lock

# Ask user to verify current base version
jhipster --version
read -p "Do you want to upgrade project to this base version? (y to continue)" yn
case $yn in
    [Yy]* ) break;;
    * ) echo "Exiting"; exit;;
esac

# Upgrade
echo "Upgrading jHipster, hold on!"
jhipster --force --with-entities || true


# Verify
yarn install
jhipster --version
